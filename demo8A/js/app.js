var temp;

var loc;

var icon;

var humidity;

var wind;

var direction;

function update(weather) {

    wind.innerHTML = weather.wind;

    direction.innerHTML = weather.direction;

    humidity.innerHTML = weather.humidity;

    loc.innerHTML = weather.loc;

    temp.innerHTML = weather.temp;

    icon.src = "img/" + weather.icon + ".jpg";

}

window.onload = function() {

    temp = document.getElementById("temperature");

    loc = document.getElementById("location");

    icon = document.getElementById("icon");

    humidity = document.getElementById("humidity");

    direction = document.getElementById("direction")

    var weather = {};

    weather.wind = 0;

    weather.direction = "SW";

    weather.humidity = 62;

    weather.loc = "San Francisco";

    weather.icon =

        update(weather);
}