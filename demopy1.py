print "Hello, world!"
#printing spacing lines is to make output easier to view when on certain terminals
print "   "
# This is the basic output line that is found in Python (version 2, in Python 3
# there is need for paratheses in the print statement)
# print("This is a python 3 print statement")

print 'You can use single quotes or double quotes for a string'
print "   "
# however it prevents the use of apostrophes
print "This is " + "a good string"
# + - additional content can be added with the addition modifier
print "   "
string = "this is a string variable"
int = 3;
print "   "
print string + " " + int
print "   "
greeting = "Welcome to Python Demo 1 "
current = 5;
print greeting + current
print "   "

# Arthmetic operations which are performed in definition and the result is value
mirthful_addition = 12381 + 91817
amazing_subtraction = 981 - 312
trippy_multiplication = 38 * 902
happy_division = 540 / 45
sassy_combinations = 129 * 1345 + 120 / 6 - 12

print mirthful_addition
print "   "
print amazing_subtraction
print "   "
print trippy_multiplication
print "   "
print happy_division
print "   "
print sassy_combinations
# modolous operator which acts to find remainder in the equations below and then printed
is_this_number_odd = 15 % 2
print is_this_number_odd
print " "
is_this_number_divisible_by_seven = 133 % 7
print is_this_number_divisible_by_seven
print " "
# below python is used to manipulate a variable representing fish in a pond
# showing variables altering value within the script and in syntax
fish_in_clarks_pond = 50

print "Catching fish"
print " "
number_of_fish_caught = 10
fish_in_clarks_pond = fish_in_clarks_pond - number_of_fish_caught
print "Fish Left: " + fish_in_clarks_pond
print " "
# a common use of data tracking is monetary as the below demonstration simply shows
# introducing assignment operators much the same as other scripting languages
money_in_wallet = 40
sandwich_price = 7.50
sales_tax = .08 * sandwich_price
print money_in_wallet + " is left in your wallet"
print " "
sandwich_price += sales_tax
print "Calculating Tax and Price Spent"
print " "
money_in_wallet -= sandwich_price
print money_in_wallet + " is left in your wallet"
# this section uses assignment operators to add rainfall to a total while printing
# each stage as it is Calculated
january_to_june_rainfall = 1.93 + 0.71 + 3.53 + 3.41 + 3.69 + 4.50
annual_rainfall = january_to_june_rainfall
print "Rainfall in the first half of the year totalled " + annual_rainfall + "Inches"
print " "
july_rainfall = 1.05
annual_rainfall += july_rainfall
print "As of July totals stood at " + annual_rainfall
print " "
august_rainfall = 4.91
annual_rainfall += august_rainfall
print "As of August totals stood at " + annual_rainfall
print " "
september_rainfall = 5.16
october_rainfall = 7.20
november_rainfall = 5.06
december_rainfall = 4.06
annual_rainfall += september_rainfall
print "As of September totals stood at " + annual_rainfall
print " "
annual_rainfall += october_rainfall
print "As of October totals stood at " + annual_rainfall
print " "
annual_rainfall += november_rainfall
print "As of November totals stood at " + annual_rainfall
print " "
annual_rainfall += december_rainfall
print "As of December totals stood at " + annual_rainfall
print " "
# variable classes are defined by value as automatic assignment unlike declared type
# more like JS than C++
# three examples of each types and types below are integers (whole numbers)
# float which is a decimal definition and
# an example of float as scientific notation for large scales which all is
# printed to terminal
intx = 1
inty = 2
intz = 3
print "Integers are numbers like " +intx + " " + inty + " " + intz
print " "
float1 = 1.0
float2 = 0.1
float3 = 6.76
print "Floating values are decimal point defined fractional values like " + float1 + " " + float2 + " " + float3
print " "
# manipulating variables by multiplying different types to test the conversion capacity of dynamic types
cucumbers = 33
print "You ordered " + cucumbers + " cucumbers"
print " "
price_per_cucumber = 3.25
print "each costing " + price_per_cucumber + " USD"
total_cost = cucumbers * price_per_cucumber
print " "
print "Your total purchase was " + total_cost + " USD"
print " "

quotient = 6/2
print quotient
print " "
print "the value of quotient is now 3, which makes sense as integers will not shift data types"
print " "
quotient = 7/2
print "the value of quotient is 3, even though the result of the division here is 3.5 due to lack of float memory allocation"
print " "
quotient1 = 7./2
# the value of quotient1 is 3.5
quotient2 = 7/2.
# the value of quotient2 is 3.5
quotient3 = 7./2.
# the value of quotient3 is 3.5
#below is the float method used to force a float result
quotient1 = float(7)/2
# the value of quotient1 is 3.5

# float method to force ints to float to get correct result in decimal
cucumbers = 100
num_people = 6
whole_cucumbers_per_person = cucumbers/num_people
float_cucumbers_per_person = float(cucumbers)/num_people
print float_cucumbers_per_person
print " "

# While ugly and not my text formatting preference here is a multiline string
# composed with triple """multiline within """

address_string = """136 Whowho Rd
Apt 7
Whosville, WZ 44494"""
# here is an annoying haiku printed for you
haiku = """The old pond,
A frog jumps in:
Plop! """

print haiku
# boolean variables hold 0 or 1 binary signals as true 1 and false 0
a = True
b = False

# string also has a function
age = 13
print "I am " + str(age) + " years old!"
print " "

number1 = "100"
number2 = "10"

string_addition = number1 + number2
#string_addition now has a value of "10010"

int_addition = int(number1) + int(number2)
#int_addition has a value of 110

# below are string function examples
string_num = "7.5"
print int(string_num)
print " "
print float(string_num)
print " "

float_1 = 0.25
float_2 = 40.0
product = float_1 * float_2
big_string = "The product was " + str(product)

# short tip calculator for a set price meal
meal = 44.50
tax = (6.75/100)
tip = 15.0
total = meal + (meal*tax)
print total
print " "
print "Goodbye World!"
