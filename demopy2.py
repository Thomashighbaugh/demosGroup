name = "Ryan"
age = "19"
food = "cheese"

# must escape apostrophes

print "This isn\'t flying, this is falling with style!"

# prints string length
parrot = "Norwegian Blue"
print len(parrot)
