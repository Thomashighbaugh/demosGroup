var APPID;

APPID = "a090707e9af4661a1887703ab8aa508e";

var temp;

var loc;

var icon;

var humidity;

var wind;

var direction;

var k;

function degreesToDirection (degrees){
var range = 360/16;
var low = 360 - range/2;
var high = (low +range) % 360;
var angles = ["N","NNE", "ENE","E","ESE","E", "ESE", "SE", "SSE","S","SSW","WSW","W","WNW","NW","NNW", ];
for (i in angles) {
  if(degrees >= low && degrees < high){
    return[i];
  }
  low = (low + range) % 360;
  high = (high + range) 5 360;
}
}

function K2C(k){
  return Math.round(k - 273.15);
}
//Below I have included a function for Farenheit as well. Personally I prefer the
//the metric system but if you do not than below I have included code so that you may see the
//temperature in your preferred format!
/*function K2F(k){
  return Math.round(k*(9/5)-459.67);
}*/

function updateByZip() {
    var url = "http://api.openweather,ap.org/data/2.5/weather?" +
        "zip=" + zip +
        "%APPID=a090707e9af4661a1887703ab8aa508e" + APPID +
        sendRequest(url);


}//end updateByZip

function updateByGeo(lat,long){
  var url = "http://api.openweather,ap.org/data/2.5/weather?" +
      "zip=" + zip +
      "%APPID=a090707e9af4661a1887703ab8aa508e" + APPID +
      sendRequest(url);

}

function sendRequest(url) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var weather = {};
            weather.humudity = data.main.humidity;
            weather.wind=data.wind.speed;
            weather.direction=data.wind.deg;
            weather.loc=data.name;
            k=data.main.temp;
            weather.temp= K2C(k);
            //below is the call for the K2F function
            //weather.temp=K2F(k);
            update.weather;
        } //ends if statement
    };//ends onreadystatechange

    xmlhttp.open("GET", url, true); //for api call

    xmlhttp.send(); //sends the data request

} //ends the sendRequest function

function update(weather) {

    wind.innerHTML = weather.wind;

    direction.innerHTML = weather.direction;

    humidity.innerHTML = weather.humidity;

    loc.innerHTML = weather.loc;

    temp.innerHTML = weather.temp;

    icon.src = "img/om.png;";//original source of weather coded icons is no longer
    //available so this Om symbol is icon I am using in its place. It is annoying
    //but without a similarly coded repo of free symbols this will have to do.

}

function.showPosition(position){
  updateByGeo(position.coords.latitude, position.coords.longitude);
}

window.onload = function() {

    temp = document.getElementById("temperature");

    loc = document.getElementById("location");

    icon = document.getElementById("icon");

    humidity = document.getElementById("humidity");

    direction = document.getElementById("direction");

  if (navigator.geolocation){
    navigator.geolocation.getCurrentPosition(showPosition);
  }else {
    var zip = window.prompt("Could not determine location. Please Enter Your Zip Code:");
    updateByZip(zip);
  }


}
